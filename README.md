
# Dropoff: aplicación web para recolección y gestión de residuos orgánicos utilizando módulo RFID para cliente Sr. Compost
## **Estado:** Prototipo. Primera iteración


*Dropoff Sr. Compost* es una aplicación web desarrollada en python para gestionar la recolección de residuos orgánicos domiciliarios, utilizando placa *Rasbperry Pi* y módulo RFID *RC522*. La arquitectura corresponde a un servidor desarrollado en Flask que genera interacción con las bases de datos alojadas en él.
Hasta el momento, la plataforma tiene las siguientes funcionalidades:

- Registrar identificador rfid, nombre del usuario, dirección, teléfono y cantidad de créditos en una base de datos.
- Consultar datos de nombre, dirección teléfono y créditos del usuario.
- Decontar créditos a cada usuario, según el servicio solicitado.
- Agregar créditos a cada usuario, según alternativas definidas.


### Hardware

- Raspberry Pi modelo 3b+
- 18650 UPS Pro
- Batería 5V 1800AH
- Módulo RFID RC522
- Etiquetas NFC
- Llaveros
- Touchscreen 7"
- LED Verde
- LED Rojo
- Resistencia 100 Ohmios

### Pinout

#### RFID-RC522

- SDA -> GPIO 8
- SCK -> GPIO 11
- MOSI -> GPIO 10
- MISO -> GPIO 9
- GND -> GND
- 3,3V -> 3,3V
- RST -> GPIO 25

#### Touchscreen 7"

- GND -> GND
- 5V -> 5V
- SCL -> GPIO 3
- SDA -> GPIO 2

#### LED

- Verde -> GPIO 18
- Rojo -> GPIO 17

### Requisitos básicos de instalación

- Raspberry Pi OS
- Python 3.5 <

### Instalación

- Clonar el repositorio desde  ` https://gitlab.com/urbanatika/rfid-compost.git`
- Instrucciones de instalación en INSTALACION.txt

### Iniciar servidor flask como servicio

Cree un archivo `dropoff.service ` en el directorio `/etc/systemd/system/ ` y agregue el siguiente contenido:

```

[Unit]
Description=Aplicacion dropoff sr compost
After=network.target

[Service]
User=pi
workingDirectory=/home/pi/rfid-compost/dropoff/
Environment="FLASK_APP=run.py"
Environment="FLASK_ENV=development"
Environment="PATH=/home/pi/rfid-compost/dropoff/venv/bin/activate"
ExecStart=/usr/bin/python3.7 -m flask run --host 127.0.0.1
Restart=always

[Install]
WantedBy=multi-user.target


```

Una vez guardado el archivo, se debe refrescar la configuración con:

```
sudo systemctl daemon-reload
```

Se puede verificar el estado del servicio con los siguientes comandos:

```
sudo systemctl start dropoff
sudo systemctl restart dropoff
sudo systemctl stop dropoff
sudo systemctl status dropoff
```

### Iniciar en modo "Kiosko"

El modo kiosko se activa como un servicio en la Raspberry. Para esto:

Y agregue: 

```

@xset s off
@xset -dpms
@xset s noblank
@unclutter -idle 0
@chromium-browser --kiosk http://127.0.0.1:5000/


```

