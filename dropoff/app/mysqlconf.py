from app import app
from flask_mysqldb import MySQL
#from flaskext.mysql import MySQL

app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DB']="db_canino"

mysql=MySQL(app)

#Funciones para base de datos de administración de usuarios


def InsertarUsuario(ingreso_rfid,NombreUsuario,SexoUsuario,EdadUsuario,Direccion,VillaZona,Telefono,Email,NombreMascota,EdadMascota,SexoMascota,EstadoReproductivo,RazaMascota,VacunaSextuple,VacunaAntirrabica,Comentarios):
	cur=mysql.connection.cursor()
	cur.execute("INSERT INTO data_usuarios (rfid,nombre_usuario,sexo_usuario,edad_usuario,direccion,villa_zona,telefono,email,nombre_mascota,edad_mascota,sexo_mascota,estado_reproductivo,raza_mascota,vacuna_sextuple,vacuna_antirrabica,comentarios) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ingreso_rfid,NombreUsuario,SexoUsuario,EdadUsuario,Direccion,VillaZona,Telefono,Email,NombreMascota,EdadMascota,SexoMascota,EstadoReproductivo,RazaMascota,VacunaSextuple,VacunaAntirrabica,Comentarios))
	mysql.connection.commit()

def ConsultarUsuario(consulta_rfid):
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM data_usuarios WHERE rfid=%s",(consulta_rfid,))
	consulta=cur.fetchall()
	return consulta
	
#def DecontarDropoff(decontar_rfid):
#	cur=mysql.connection.cursor()
#	cur.execute("UPDATE data_usuarios SET creditos=creditos-2000 WHERE rfid='%s'",[decontar_rfid])
#	mysql.connection.commit()
#	cur2=mysql.connection.cursor()
#	cur2.execute("SELECT * FROM data_usuarios WHERE rfid='%s'",[decontar_rfid])
#	resultado=cur2.fetchall()
#	return resultado

def CargaCreditos(cred,cargar_rfid):
	cur=mysql.connection.cursor()
	cur.execute("UPDATE data_usuarios SET comentarios=%s WHERE rfid=%s",(cred,cargar_rfid) )
	mysql.connection.commit()
	
	#cur=mysql.connection.cursor()
	#cur.execute("UPDATE data_usuarios SET creditos=creditos+'%s' WHERE rfid='%s'",(cred,cargar_rfid))
	#mysql.connection.commit()
	
def ConsultarRfid():
	cur=mysql.connection.cursor()
	cur.execute("SELECT rfid FROM data_usuarios")
	rfid=cur.fetchall()
	return rfid
	

def ConsultaGlobal(criterio,data):
	cur=mysql.connection.cursor()
	if criterio=="nombre":
		cur.execute("SELECT * FROM data_usuarios WHERE nombre_usuario=%s",(data.upper(),))
		consulta=cur.fetchall()
		return consulta
	elif criterio=="sexo":
	    cur.execute("SELECT * FROM data_usuarios WHERE sexo_usuario=%s",(data.upper(),))
	    consulta=cur.fetchall()
	    return consulta
	elif criterio=="correo":
		cur.execute("SELECT * FROM data_usuarios WHERE email=%s",(data.upper(),))
		consulta=cur.fetchall()
		return consulta
	
	elif criterio=="direccion":
		cur.execute("SELECT * FROM data_usuarios WHERE direccion=%s",(data.upper(),))
		consulta=cur.fetchall()
		return consulta
	elif criterio=="todos":
		cur.execute("SELECT * FROM data_usuarios")
		consulta=cur.fetchall()
		return consulta
	
	
def CantidadUsuarios():
	cur=mysql.connection.cursor()
	cur.execute("SELECT COUNT(*) id FROM data_usuarios")
	cantidad=cur.fetchone()
	return cantidad

#def TotalCreditos():
#	cur=mysql.connection.cursor()
#	cur.execute("SELECT SUM(creditos) FROM data_usuarios")
#	total=cur.fetchone()
#	return total
	
def EliminarUsuario(id):
	cur=mysql.connection.cursor()
	cur.execute("DELETE FROM data_usuarios WHERE id=%s",(id,))
	mysql.connection.commit()
	
def SeleccionarUsuario(id):
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM data_usuarios WHERE id=%s",(id,))
	data=cur.fetchall()
	return data[0]

def SeleccionarPrimerUsuario():
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM data_usuarios ORDER BY id ASC LIMIT 1")
	primer_usuario=cur.fetchall()
	return primer_usuario
	
def SeleccionarUltimoUsuario():
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM data_usuarios ORDER BY id DESC LIMIT 1")
	ultimo_usuario=cur.fetchall()
	return ultimo_usuario
	
	


#def EditarUsuario(id,act_nombre,act_sexo,act_correo,act_edad,act_direccion,act_telefono,act_creditos):
#	cur=mysql.connection.cursor()
#	cur.execute("UPDATE data_usuarios SET nombre=%s,sexo=%s,correo=%s,edad=%s,direccion=%s,telefono=%s,creditos=%s WHERE id=%s", (act_nombre,act_sexo,act_correo,act_edad,act_direccion,act_telefono,act_creditos,id))
#	mysql.connection.commit()
	
def EditarUsuario(id,ActNombreUsuario,ActSexoUsuario,ActEdadUsuario,ActDireccion,ActVillaZona,ActTelefono,ActEmail,ActNombreMascota,ActEdadMascota,ActSexoMascota,ActEstadoReproductivo,ActRazaMascota,ActVacunaSextuple,ActVacunaAntirrabica,ActComentarios):
	cur=mysql.connection.cursor()
	cur.execute("UPDATE data_usuarios SET nombre_usuario=%s,sexo_usuario=%s,edad_usuario=%s,direccion=%s,villa_zona=%s,telefono=%s,email=%s,nombre_mascota=%s,edad_mascota=%s,sexo_mascota=%s,estado_reproductivo=%s,raza_mascota=%s,vacuna_sextuple=%s,vacuna_antirrabica=%s,comentarios=%s WHERE id=%s",(ActNombreUsuario,ActSexoUsuario,ActEdadUsuario,ActDireccion,ActVillaZona,ActTelefono,ActEmail,ActNombreMascota,ActEdadMascota,ActSexoMascota,ActEstadoReproductivo,ActRazaMascota,ActVacunaSextuple,ActVacunaAntirrabica,ActComentarios,id))
	mysql.connection.commit()

def CanjearBeneficios(id):
	cur=mysql.connection.cursor()
	cur.execute("UPDATE data_usuarios SET comentarios=%s WHERE id=%s",("USUARIO NO POSEE BENEFICIOS",id) )
	mysql.connection.commit()
