import sys
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12,GPIO.OUT)

buzzerpin=32

def Success():
	melodia_success=[520]
	tempo_success=[5]

	setup()
	play(melodia_success,tempo_success,0.5,1)
	time.sleep(2)
	#destroy()
	

def Fail():
	
	melodia_fail=[440,440,440]
	tempo_fail=[5,5,5]
	setup()
	play(melodia_fail, tempo_fail,0.6,1.5)
	time.sleep(2)
	#destroy()



def buzz(frecuencia,largo):
	if (frecuencia==0):
		time.sleep(largo)
		return
	periodo=1.0/frecuencia
	delay=periodo/2
	ciclos=int(largo*frecuencia)
	for i in range(ciclos):
		GPIO.output(buzzerpin,True)
		time.sleep(delay)
		GPIO.output(buzzerpin,False)
		time.sleep(delay)

def setup():

	#GPIO.setup(buzzerpin,GPIO.IN)
	GPIO.setup(buzzerpin,GPIO.OUT)

def destroy():
	GPIO.cleanup()

def play(melodia,tempo,pausa,pace=0.800):
	for i in range(0,len(melodia)):
		duracionnota=pace/tempo[i]
		buzz(melodia[i],duracionnota)
		pausaentrenotas=duracionnota*pausa
		time.sleep(pausaentrenotas)
		
