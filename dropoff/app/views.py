#!/usr/bin/env python

from app import app, mysqlconf, buzzer, rfid, memoria

#from app import rfid
#from app.mfrc522 import MFRC522

from flask import render_template, request, url_for, flash, redirect
from werkzeug.exceptions import BadRequestKeyError


import time
import RPi.GPIO as GPIO


#GPIO.setmode(GPIO.BOARD)
#GPIO.setup(12,GPIO.OUT)
#GPIO.setup(11,GPIO.OUT)

#GPIO.setmode(GPIO.BCM)
	#GPIO.setup(buzzerpin,GPIO.IN)
#GPIO.setup(32,GPIO.OUT)


app.secret_key="secretkey"





@app.route("/",methods=["GET","POST"])
def Index():
	
	return render_template("index.html")
	
	
@app.route("/ingreso",methods=["GET","POST"])
def Ingreso():
	
	#obtiene data del lector RFID
	data=rfid.ReadRfid2()
	#si la data es distinta de cero (la etiqueta tiene asociado un número, formateado en la función RFID)
	if data!="0":
		
		#se asigna el valor de la etiqueta formateado como string, obtenido de la base de datos 
		verificar_rfid=str(mysqlconf.ConsultarRfid())
		#si el valor del lector RFID coincide con el de la etiqueta
		if (data in verificar_rfid):
			flash("El usuario ya existe")
			return redirect(url_for("Index"))
		else:
			#en caso que no exista, se lleva a la ruta de ingreso de usuario
			return render_template("ingreso.html",data=data)
	
	#en caso que la data sea cero (no se lea la etiqueta)
	else:
		flash("No existe llavero")
		#redirige al index
		return redirect(url_for("Index"))	
	
@app.route("/ingresardatos",methods=["GET","POST"])
def IngresoDatos():
	buzzer.Success()
	#obtiene la data del lector RFID
	text=rfid.ReadRfid2()
	
	
	try:
		#si el request es un post:
		if request.method=="POST":
			#se obtienen los datos del usuario
			nombre_usuario=request.form["nombre_usuario"]
			sexo_usuario=request.form["sexo_usuario"]
			edad_usuario=request.form["edad_usuario"]
			direccion=request.form["direccion"]
			villa_zona=request.form["villa_zona"]
			telefono=request.form["telefono"]
			email=request.form["email"]
			#se obtienen los datos de la mascota
			nombre_mascota=request.form["nombre_mascota"]
			edad_mascota=request.form["edad_mascota"]
			sexo_mascota=request.form["sexo_mascota"]
			estado_reproductivo=request.form["estado_reproductivo"]
			raza_mascota=request.form["raza_mascota"]
			vacuna_sextuple=request.form["vacuna_sextuple"]
			vacuna_antirrabica=request.form["vacuna_antirrabica"]
			comentarios=request.form["comentarios"]
			#se asigna el valor obtenido del lector RFID
			ingreso_rfid=text
			#se inserta un usuario en la base de datos
			mysqlconf.InsertarUsuario(ingreso_rfid,nombre_usuario.upper(),sexo_usuario.upper(),edad_usuario,direccion.upper(),villa_zona.upper(),telefono,email.upper(),nombre_mascota.upper(),edad_mascota,sexo_mascota.upper(),estado_reproductivo.upper(),raza_mascota.upper(),vacuna_sextuple.upper(),vacuna_antirrabica.upper(),comentarios.upper())
			
			msg=str("Usuario "+nombre_usuario+" agregado exitosamente")
			flash(msg) 
			#redirige al index
		return redirect(url_for("Index"))
	
	#validación en caso de que no se ingresen todos los datos
	except BadRequestKeyError as e:
		
		if e:
			flash("No ha ingresado todos sus datos")
			return redirect(url_for("Ingreso"))


#////////////PENDIENTE: DESARROLLAR COMPARATIVA DE STRING. DADO QUE SE CONSULTA POR NOMBRE COMPLETO, FILTRAR POR NOMBRE O APELLIDO //////////

@app.route("/consultar", methods=["GET","POST"])
def Consultar():
	#obtiene la data del lector RFID
	text=rfid.ReadRfid2()
	#si la lectura es distinta de 0
	if text!="0":
		#se transforma en string el valor obtenido de la data
		consulta_rfid=str(text)
		#se consulta el usuario en la base de datos y se le asigna a la variable consulta
		consulta=mysqlconf.ConsultarUsuario(consulta_rfid)
		return render_template("consulta.html",consulta=consulta)
	#en caso que la lectura sea 0, no existe llavero
	else:
		flash("no existe llavero")
		return redirect(url_for("Index"))

#//////////FIN PENDIENTE ///////////
	
@app.route("/beneficios",methods=["GET","POST"])
def Beneficios():
	#obtiene la data del lector RFID
	text=rfid.ReadRfid2()
	#se convierte el dato de RFID en string
	data=str(text)
	#se consulta la data en la base de datos
	verificar_rfid=str(mysqlconf.ConsultarRfid())
	
	if not data in verificar_rfid:
		flash("El usuario no ha sido ingresado")
		return redirect(url_for("Index"))
	else:
		resultado=mysqlconf.ConsultarUsuario(data)
		
		return render_template("beneficios.html",resultado=resultado)
	
	
	


@app.route("/canjearbeneficios/<int:id>",methods=["GET","POST"])
def CanjearBeneficios(id):
	buzzer.Success()
	text=rfid.ReadRfid2()
	mysqlconf.CanjearBeneficios(id)
	
	return redirect(url_for("Beneficios"))


	
@app.route("/cargar",methods=["GET","POST"])
def Cargar():
	
	data=rfid.ReadRfid2()
			
	
	return render_template("carga.html",data=data)

#//////////// PENDIENTE: TRASFORMAR FUNCION EN CARGAR BENEFICIOS //////////

	
@app.route("/cargarcreditos", methods=["GET","POST"])
def CargarCreditos():
	try:
	
		text=rfid.ReadRfid2()
		
		if request.method=="POST":
			com=request.form["comentarios"]
			com=com.upper()
			cargar_rfid=text
			mysqlconf.CargaCreditos(com,cargar_rfid)
			
			buzzer.Success()
			
			
			flash("Beneficios Actualizados")
		
		return redirect(url_for("Index"))
	except:
		flash("El usuario no ha sido ingresado")
		return render_template("index.html")	
	

#RUTAS PARA VISUALIZACIÓN DE MÉTRICAS Y DATOS

@app.route("/admin/visualizacion", methods=["GET","POST"])
def VisualizacionAdmin():
	consulta_admin=""
	if request.method=="POST":
		criterio=str(request.form["criterio"])		
		data=str(request.form["data"])		
		print(data)
		consulta_admin=mysqlconf.ConsultaGlobal(criterio,data)
	return render_template("admin.html", consulta_admin=consulta_admin)

@app.route("/admin/indicadores",methods=["GET","POST"])
def VisualizacionIndicadores():
	q_usuarios=mysqlconf.CantidadUsuarios()
	#t_creditos=mysqlconf.TotalCreditos()
	
	return render_template("indicadores.html",q_usuarios=q_usuarios[0])

@app.route("/admin/visualizacion/eliminar/<string:id>", methods=["GET","POST"])
def Eliminar(id):
	mysqlconf.EliminarUsuario(id)
	flash("Usuario removido satisfactoriamente")
	return redirect(url_for("VisualizacionAdmin"))
	



@app.route("/admin/visualizacion/editar/<id>", methods=["GET","POST"])
def Obtener(id):
	data=mysqlconf.SeleccionarUsuario(id)
	return render_template("editarusuario.html", usuario=data)

@app.route("/admin/visualizacion/actualizar/<id>",methods=["GET","POST"])
def Actualizar(id):
	try:
		if request.method=="POST":
			#act_nombre=request.form["nombre_usuario"]
			#act_sexo=request.form["sexo_usuario"]
			#act_correo=request.form["correo"]
			#act_edad=request.form["edad"]
			#act_direccion=request.form["direccion"]
			#act_telefono=request.form["telefono"]
			#act_creditos=request.form["creditos"]
			
			#se obtienen los datos del usuario
			act_nombre_usuario=request.form["nombre_usuario"]
			act_sexo_usuario=request.form["sexo_usuario"]
			act_edad_usuario=request.form["edad_usuario"]
			act_direccion=request.form["direccion"]
			act_villa_zona=request.form["villa_zona"]
			act_telefono=request.form["telefono"]
			act_email=request.form["email"]
			#se obtienen los datos de la mascota
			act_nombre_mascota=request.form["nombre_mascota"]
			act_edad_mascota=request.form["edad_mascota"]
			act_sexo_mascota=request.form["sexo_mascota"]
			act_estado_reproductivo=request.form["estado_reproductivo"]
			
			act_raza_mascota=request.form["raza_mascota"]
			act_vacuna_sextuple=request.form["vacuna_sextuple"]
			act_vacuna_antirrabica=request.form["vacuna_antirrabica"]
			act_comentarios=request.form["comentarios"]
			
			
			
			mysqlconf.EditarUsuario(id,act_nombre_usuario.upper(),act_sexo_usuario.upper(),act_edad_usuario,act_direccion.upper(),act_villa_zona.upper(),act_telefono,act_email.upper(),act_nombre_mascota.upper(),act_edad_mascota,act_sexo_mascota.upper(),act_estado_reproductivo.upper(),act_raza_mascota.upper(),act_vacuna_sextuple.upper(),act_vacuna_antirrabica.upper(),act_comentarios.upper())
			#mysqlconf.EditarUsuario(id,act_nombre.upper(),act_sexo.upper(),act_correo.upper(),act_edad,act_direccion.upper(),act_telefono,act_creditos)
			flash("Usuario actualizado satisfactoriamente")
			return redirect(url_for("VisualizacionAdmin"))
	except BadRequestKeyError as e:
		
		if e:
			flash("No ha ingresado todos sus datos. Debe actualizar obligadamente bien las listas desplegables")
			return render_template("admin.html")
			
		
@app.route("/admin/visualizacion/respaldar",methods=["GET","POST"])
def Respaldar():
	try:
		
		#almacena la primera fila de la base de datos y asigna la variable id
		primera_posicion=mysqlconf.SeleccionarPrimerUsuario()
		primera_posicion=primera_posicion[0][0]
		#almacena la última fila de la base de datos y asigna la variable id
		ultima_posicion=mysqlconf.SeleccionarUltimoUsuario()
		ultima_posicion=ultima_posicion[0][0]
		
		
		#fila_usuario=mysqlconf.SeleccionarUsuario(primera_posicion)
		#fila_usuario=fila_usuario
		
		#Crea el archivo csv y escribe el encabezado del documento
		memoria.CrearCsv()
		memoria.EncabezadoCsv()
		
		#recorre toda la base de datos entre la primera y ultima id, y escribe los datos del usuario en cada linea del documento
		for i in range(primera_posicion,ultima_posicion+1):
			fila_usuario=mysqlconf.SeleccionarUsuario(i)
			memoria.ArchivarCsv(fila_usuario)
		
		flash("Datos almacenados exitosamente en 'archivo_csv.csv'")
		return redirect(url_for("VisualizacionAdmin"))
	except OSError as e:
		#en caso de no introducir un USB válido, entrega una alerta al usuario.
		flash("Dispositivo USB erróneo o inexistente. Insertar dispositivo T_RESP_USB e intentar nuevamente")
		return redirect(url_for("VisualizacionAdmin"))
