-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-09-2021 a las 13:22:06
-- Versión del servidor: 10.3.29-MariaDB-0+deb10u1
-- Versión de PHP: 7.3.29-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_canino`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_usuarios`
--

CREATE TABLE `data_usuarios` (
  `id` int(11) NOT NULL,
  `rfid` int(11) NOT NULL,
  `nombre_usuario` text NOT NULL,
  `sexo_usuario` text NOT NULL,
  `edad_usuario` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `villa_zona` text NOT NULL,
  `telefono` int(15) NOT NULL,
  `email` text NOT NULL,
  `nombre_mascota` text NOT NULL,
  `edad_mascota` text NOT NULL,
  `sexo_mascota` text NOT NULL,
  `estado_reproductivo` text NOT NULL,
  `raza_mascota` text NOT NULL,
  `vacuna_sextuple` text NOT NULL,
  `vacuna_antirrabica` text NOT NULL,
  `comentarios` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `data_usuarios`
--
ALTER TABLE `data_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `data_usuarios`
--
ALTER TABLE `data_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
